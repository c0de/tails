# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-03-13 13:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install additional software\"]]\n"
msgstr "[[!meta title=\"Installer des logiciels additionnels\"]]\n"

#. type: Plain text
msgid ""
"Tails includes a [[coherent but limited set of applications|doc/about/"
"features]]. More applications can be installed as on any Debian system. Only "
"applications that are packaged for Debian can be installed. To know if an "
"application is packaged for Debian, and to find the name of the "
"corresponding software packages, you can search for it in the [[Debian "
"package directory|https://www.debian.org/distrib/packages]]."
msgstr ""
"Tails inclus un [[ensemble cohérent mais limité d'applications|doc/about/"
"features]]. D'autres logiciels peuvent être installés, comme sur tout "
"système basé sur Debian. Notez que seuls les logiciels empaquetés pour "
"Debian peuvent être installés. Pour savoir si un logiciel est empaqueté pour "
"Debian, et pour trouver le nom du paquet logiciel correspondant, vous pouvez "
"effectuer une recherche sur l'[[annuaire de paquet de Debian|https://www."
"debian.org/distrib/packages]]."

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The packages included in Tails are carefully tested for security.\n"
"Installing additional packages might break the security built in Tails.\n"
"Be careful with what you install.</p>\n"
msgstr ""
"<p>La sécurité des logiciels inclus dans Tails est soigneusement testée.\n"
"Installer des paquets supplémentaires peut mettre en péril la sécurité de votre Tails.\n"
"Soyez donc prudent lorsque vous installez un paquet supplémentaire.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"Since Tails is amnesic, any additional software package needs to be reinstalled in each working\n"
"session. To install the same software packages automatically at the beginning of every working session use the\n"
"[[<span class=\"guilabel\">Additional software packages</span> persistence feature|doc/first_steps/persistence/configure#additional_software]] instead.\n"
msgstr ""
"Tails est amnésique. En conséquence, un logiciel additionnel doit être réinstallé après chaque redémarrage.\n"
"Pour réinstaller automatiquement un logiciel à chaque démarrage de Tails, vous devez utiliser la\n"
"[[fonction de persistance <span class=\"guilabel\">Logiciels additionnels</span>|doc/first_steps/persistence/configure#additional_software]].\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>Packages that use the network need to be configured to go through Tor. They are otherwise blocked from accessing the network.</p>\n"
msgstr "<p>Les logiciels utilisant le réseau doivent être configurés pour passer par Tor. Dans le cas contraire, ils ne pourront pas accéder au réseau.</p>\n"

#. type: Plain text
msgid "To install additional software packages:"
msgstr "Pour installer des logiciels additionnels :"

#. type: Bullet: '1. '
msgid ""
"[[Set up an administration password|doc/first_steps/startup_options/"
"administration_password]]."
msgstr ""
"[[Configurez un mot de passe administrateur|doc/first_steps/startup_options/"
"administration_password]]."

#. type: Bullet: '2. '
msgid ""
"Open a [[root terminal|doc/first_steps/startup_options/"
"administration_password#open_root_terminal]]."
msgstr ""
"Ouvrez un [[terminal administrateur|doc/first_steps/startup_options/"
"administration_password#open_root_terminal]]."

#. type: Bullet: '3. '
msgid ""
"Execute the following command to update the lists of available packages:"
msgstr ""
"Exécutez la commande suivante pour mettre à jour la liste des paquets "
"disponibles :"

#. type: Plain text
#, no-wrap
msgid "       apt-get update\n"
msgstr "       apt-get update\n"

#. type: Bullet: '3. '
msgid ""
"To install an additional package, execute the following command, replacing `"
"[package]` with the name of the package that you want to install:"
msgstr ""
"Pour installer un logiciel additionnel, exécutez la commande suivante, en "
"remplaçant `[paquet]` par le nom du paquet que vous souhaitez installer :"

#. type: Plain text
#, no-wrap
msgid "       apt-get install [package]\n"
msgstr "       apt-get install [paquet]\n"

#. type: Plain text
#, no-wrap
msgid "   For example, to install the package `ikiwiki`, execute:\n"
msgstr "   Par exemple, pour installer le paquet `ikiwiki`, exécutez la commande suivante:\n"

#. type: Plain text
#, no-wrap
msgid "       apt-get install ikiwiki\n"
msgstr "       apt-get install ikiwiki\n"

#. type: Plain text
#, no-wrap
msgid "   <div class=\"note\">\n"
msgstr "   <div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p>You can also write multiple package names to install several packages at the same\n"
"   time. If a package has dependencies, those will be installed\n"
"   automatically.</p>\n"
msgstr ""
"   <p>Vous pouvez également écrire le nom de plusieurs paquets, pour installer plusieurs\n"
"   paquets en même temps. Si un paquet a des dépendances, elles seront automatiquement\n"
"   installées.</p>\n"

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr "   </div>\n"
