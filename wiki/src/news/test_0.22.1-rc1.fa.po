# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-26 13:54+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian "
"<http://weblate.451f.org:8889/projects/tails/test_0221-rc1/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Call for testing: 0.22.1~rc1\"]]\n"
msgstr "[[!meta title=\"درخواست آزمایش: 0.22.1~rc1\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"2014-01-11 00:18:00\"]]\n"
msgstr "[[!meta date=\"۲۰۱۴/۰۱/۱۱ ۰۰:۱۸:۰۰\"]]\n"

#. type: Plain text
msgid ""
"You can help Tails! The first release candidate for the upcoming version "
"0.22.1 is out. Please test it and see if it works for you."
msgstr ""
"شما می‌توانید به تیلز کمک کنید! اولین نامزد انتشار برای نسخهٔ پیش‌روی ۰٫۲۲٫۱ "
"آماده شده‌است. لطفاً آن را امتحان کنید و ببینید برایتان کار می‌کند یا نه."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, no-wrap
msgid "How to test Tails 0.22.1~rc1?\n"
msgstr "چگونه تیلز 0.22.1~rc1 را امتحان کنیم؟\n"

#. type: Bullet: '1. '
msgid ""
"**Keep in mind that this is a test image.** We have made sure that it is not "
"broken in an obvious way, but it might still contain undiscovered issues."
msgstr ""
"**در خاطر داشته باشید که این یک تصویر آزمایشی است.** مطمئن شده‌ایم که هیچ "
"آسیب مشهودی ندارد، اما همچنان ممکن است مشکلاتی شناسایی‌نشده داشته باشد."

#. type: Bullet: '2. '
msgid ""
"Either try the <a href=\"#automatic_upgrade\">automatic upgrade</a>, or "
"download the ISO image and its signature:"
msgstr ""
"یا از <a href=\"#automatic_upgrade\">ارتقای خودکار</a> استفاده کرده یا تصویر "
"ایزو و امضای آن را دانلود کنید:"

#. type: Plain text
#, no-wrap
msgid ""
"   <a class=\"download-file\" "
"href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-0.22.1~rc1/tails-i386-0.22.1~rc1.iso\" "
">Tails 0.22.1~rc1 ISO image</a>\n"
msgstr ""
"   <a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/t"
"ails-i386-0.22.1~rc1/tails-i386-0.22.1~rc1.iso\" >تصویر ایزوی تیلز 0.22.1~"
"rc1 </a>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <a class=\"download-signature\"\n"
"   "
"href=\"https://tails.boum.org/torrents/files/tails-i386-0.22.1~rc1.iso.sig\">Tails "
"0.22.1~rc1 signature</a>\n"
msgstr ""
"   <a class=\"download-signature\"\n"
"   href=\"https://tails.boum.org/torrents/files/tails-i386-0.22.1~rc1.iso."
"sig\">امضای تیلز 0.22.1~rc1</a>\n"

#. type: Bullet: '1. '
msgid "[[Verify the ISO image|download#verify]]."
msgstr "[[تأیید تصویر ایزو|download#verify]]."

#. type: Bullet: '1. '
msgid ""
"Have a look at the list of <a href=\"#known_issues\">known issues of this "
"release</a> and the list of [[longstanding known "
"issues|support/known_issues]]."
msgstr ""
"نگاهی به فهرست <a href=\"#known_issues\">مشکلات شناسایی‌شدهٔ این انتشار</a> "
"و فهرست [[lمشکلات شناسایی‌شدهٔ قدیمی|support/known_issues]] بیندازید."

#. type: Bullet: '1. '
msgid "Test wildly!"
msgstr "به شدت آن را امتحان کنید!"

#. type: Plain text
#, no-wrap
msgid ""
"If you find anything that is not working as it should, please [[report to\n"
"us|doc/first_steps/bug_reporting]]! Bonus points if you check that it is not "
"a\n"
"<a href=\"#known_issues\">known issue of this release</a> or a\n"
"[[longstanding known issue|support/known_issues]].\n"
msgstr ""
"اگر هر چیزی یافتید که مطابق انتظار کار نمی‌کند لطفاً [[به ما گزارش بدهید|\n"
"doc/first_steps/bug_reporting]]! چه بهتر که مطمئن شده باشید\n"
"این مشکل یک <a href=\"#known_issues\">مشکل شناسایی‌شدهٔ این انتشار</a> \n"
"یا یک [[مشکل شناسایی‌شدهٔ قدیمی|support/known_issues]] نیست.\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"automatic_upgrade\"></a>\n"
msgstr "<div id=\"automatic_upgrade\"></a>\n"

#. type: Title =
#, no-wrap
msgid "How automatically upgrade from 0.22?\n"
msgstr "چگونه از ۰٫۲۲ ارتقای خودکار را انجام بدهم؟\n"

#. type: Plain text
#, no-wrap
msgid ""
"These steps allow you to automatically upgrade a device installed with "
"<span\n"
"class=\"application\">Tails Installer</span> from Tails 0.22 to Tails "
"0.22.1~rc1.\n"
msgstr ""
"این مراحل به شما اجازه می‌دهند تا به طور خودکار یک دستگاه نصب‌شده با <span\n"
"class=\"application\">نصب‌کنندهٔ تیلز</span> را از تیلز ۰٫۲۲ به تیلز 0.22.1~"
"rc1 ارتقاء دهید.\n"

#. type: Bullet: '1. '
msgid ""
"Start Tails 0.22 and [[set an administration "
"password|doc/first_steps/startup_options/administration_password]]."
msgstr ""
"تیلز ۰٫۲۲ را راه‌اندازی کرده و[[یک گذرواژهٔ مدیریتی ایجاد "
"کنید|doc/first_steps/startup_options/administration_password]]."

#. type: Bullet: '2. '
msgid ""
"Run this command in a <span class=\"application\">Root Terminal</span> to "
"select the \"alpha\" upgrade channel: <pre><code>echo "
"TAILS_CHANNEL=\\\"alpha\\\" >> /etc/os-release</code></pre>"
msgstr ""
"این فرمان را در یک <span class=\"application\">پایانهٔ اصلی</span> اجرا کرده "
"یک کانال ارتقای «آلقا» پیدا کنید‌: <pre><code>echo TAILS_CHANNEL=\\\"alpha\\"
"\" >> /etc/os-release</code></pre>"

#. type: Bullet: '3. '
msgid ""
"Run this command in a <span class=\"application\">Root Terminal</span> to "
"install the latest version of <span class=\"application\">Tails "
"Upgrader</span>:"
msgstr ""
"این فرمان را در یک <span class=\"application\">پنجرهٔ اصلی</span> اجرا کرده "
"تا آخرین نسخهٔ <span class=\"application\">ارتقاءدهندهٔ تیلز</span> نصب شود:"

#. type: Plain text
#, no-wrap
msgid ""
"       echo \"deb http://deb.tails.boum.org/ 0.22.1-rc1 main\" \\\n"
"           > /etc/apt/sources.list.d/tails-0.22-rc1.list && \\\n"
"           sed -i -e '/tor-0.2.4.x-squeeze/ d' \\\n"
"          /etc/apt/sources.list.d/torproject.list &&\n"
"           apt-get update && \\\n"
"           apt-get install tails-iuk\n"
msgstr ""
"       echo \"deb http://deb.tails.boum.org/ 0.22.1-rc1 main\" \\\n"
"           > /etc/apt/sources.list.d/tails-0.22-rc1.list && \\\n"
"           sed -i -e '/tor-0.2.4.x-squeeze/ d' \\\n"
"          /etc/apt/sources.list.d/torproject.list &&\n"
"           apt-get update && \\\n"
"           apt-get install tails-iuk\n"

#. type: Plain text
#, no-wrap
msgid ""
"4. Choose <span class=\"menuchoice\">\n"
"    <span class=\"guimenu\">Applications</span>&nbsp;&#x25B8;\n"
"    <span class=\"guisubmenu\">Tails</span>&nbsp;&#x25B8;\n"
"    <span class=\"guimenuitem\">Tails Upgrader</span>\n"
"  </span>\n"
"  to start <span class=\"application\">Tails Upgrader</span>.\n"
msgstr ""
"۴.  از فهرست <span class=\"menuchoice\">\n"
"    <span class=\"guimenu\">ابزارها</span>&nbsp;&#x25B8;\n"
"    <span class=\"guisubmenu\">تیلز</span>&nbsp;&#x25B8;\n"
"    <span class=\"guimenuitem\">ارتقاءدهندهٔ تیلز</span>\n"
"  </span>\n"
"  بروید تا <span class=\"application\">ارتقاءدهندهٔ تیلز</span> را آغاز کنید."
"\n"

#. type: Plain text
#, no-wrap
msgid ""
"5. Accept <span class=\"application\">Tails Upgrader</span>&#39;s proposal\n"
"to upgrade to Tails 0.22.1~rc1.\n"
msgstr ""
"۵. پیشنهاد <span class=\"application\">ارتقاءدهندهٔ تیلز</span>&#39;s "
"proposal\n"
"را برای ارتقاء به تیلز 0.22.1~rc1 بپذیرید.\n"

#. type: Bullet: '6. '
msgid "Wait while the upgrade is downloaded and applied."
msgstr "صبر کنید تا ارتقاء دانلود و اجرا شود."

#. type: Bullet: '7. '
msgid "Restart the system when advised to do so."
msgstr "هر گاه گفته شد رایانه را دوباره راه‌اندازی کنید."

#. type: Plain text
#, no-wrap
msgid ""
"8. Once the system is restarted, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">System</span>&nbsp;&#x25B8;\n"
"  <span class=\"guimenuitem\">About Tails</span>\n"
"</span>\n"
"to confirm that the running system is now Tails 0.22.1~rc1.\n"
msgstr ""
"۸. پس از راه‌اندازی دوبارهٔ رایانه از مسیر\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">سیستم</span>&nbsp;&#x25B8;\n"
"  <span class=\"guimenuitem\">دربارهٔ تیلز</span>\n"
"</span>\n"
"بروید تا مطمئن شوید که سیستم در حال اجرا تیلز 0.22.1~rc1 است.\n"

#. type: Title =
#, no-wrap
msgid "What's new since 0.22?\n"
msgstr "چه چیزهایی نسبت به ۰٫۲۲ جدید هستند؟\n"

#. type: Plain text
#, no-wrap
msgid ""
"- Security fixes\n"
"  - Update NSS to 3.14.5-1~bpo60+1.\n"
msgstr ""
"- رفع مشکلات امنیتی\n"
"  - ارتقای ان‌اس‌اس‌ به 3.14.5-1~bpo60+1.\n"

#. type: Plain text
#, no-wrap
msgid ""
"- Major improvements\n"
"  - Check for upgrades availability using Tails Upgrader, and propose to "
"apply an incremental upgrade whenever possible.\n"
"  - Install Linux 3.12 (3.12.6-2).\n"
msgstr ""
"- پیشرفت‌های مهم\n"
"  - گشتن برای ارتقاهای موجود با استفاده از ارتقاءدهندهٔ تیلز و پیشنهاد به "
"کار بردن ارتقاهای تدریجی در صورت امکان.\n"
"  - نصب لینوکس ۳٫۱۲ (۳٫۱۲٫۶-۲).\n"

#. type: Plain text
#, no-wrap
msgid ""
"- Bugfixes\n"
"  - Fix the keybindings problem introduced in 0.22.\n"
"  - Fix the Unsafe Browser problem introduced in 0.22.\n"
"  - Use IE's icon in Windows camouflage mode.\n"
"  - Handle some corner cases better in Tails Installer.\n"
msgstr ""
"- رفع ایرادها\n"
"  - برطرف کردن مشکل keybinding معرفی‌شده در ۰٫۲۲.\n"
"  - برطرف کردن مشکل «مرورگر غیرامن» معرفی‌شده در ۰٫۲۲.\n"
"  - استفاده از نمایهٔ اینترنت اکسپلورر در حالت استتار ویندوزی.\n"
"  - رسیدگی به بعضی مشکلات در نصب‌کنندهٔ تیلز.\n"

#. type: Plain text
#, no-wrap
msgid ""
"- Minor improvements\n"
"  - Update Tor Browser to 24.2.0esr-1+tails1.\n"
"  - Update Torbutton to 1.6.5.3.\n"
"  - Do not start Tor Browser automatically, but notify when Tor is ready.\n"
"  - Import latest Tor Browser prefs.\n"
"  - Many user interface improvements in Tails Upgrader.\n"
msgstr ""
"- بهبودهای جزیی\n"
"  - ارتقای مرورگر تور به 24.2.0esr-1+tails1.\n"
"  - ارتقای تورباتن به ۱٫۶٫۵٫۳.\n"
"  - راه‌اندازی نکردن خودکار مرورگر تور و در عوض اطلاع دادن این که تور آماده "
"است.\n"
"  - وارد کردن آخرین ترجیحات مرورگر تور.\n"
"  - بهبودهای بسیار در رابط کاربری ارتقاءدهندهٔ تیلز.\n"

#. type: Plain text
#, no-wrap
msgid ""
"See the <a "
"href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">online\n"
"Changelog</a> for technical details.\n"
msgstr ""
"برای جزییات فنی رجوع کنید به <a href=\"https://git-"
"tails.immerda.ch/tails/plain/debian/changelog?h=stable\">\n"
"لاگ آنلاین تغییرات</a>.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"known_issues\"></a>\n"
msgstr "<a id=\"known_issues\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Known issues in 0.22.1~rc1\n"
msgstr "مشکلات شناسایی‌شده در 0.22.1~rc1\n"

#. type: Bullet: '- '
msgid ""
"The memory erasure on shutdown [[!tails_ticket 6460 desc=\"does not work on "
"some hardware\"]]."
msgstr ""
"پاک شدن حافظه هنگام خاموش شدن [[!tails_ticket 6460 desc=\"روی بعضی سخت‌"
"افزارها کار نمی‌کند\"]]."
