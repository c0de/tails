[[!meta title="Calendar"]]

* 2016-03-08: Release 2.2

* 2016-04-19: Release 2.3

* 2016-05-31: Release 2.4

* 2016-07-12: Release 2.5

* 2016-08-23: Release 2.6
